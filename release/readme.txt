NTDSIM version 3.3.6 - july 10, 2024

See readme.txt under the input directory for changes regarding input.

Documentation for this program can be found at the following locations:
1 - files in this directory:
    * readme.txt, general information and how to start the program
    * changelog.txt, changes since previous versions.
    * /input/readme.txt, info on changes in input
    * input files in /input are example files with many explanatory comments.
2 - the wiki for this project, at 
https://survey.erasmusmc.nl/ntd/wiki/doku.php?id=mantis:NTDsim:start. 
Specifically the specifications part, which can be found at
https://survey.erasmusmc.nl/ntd/wiki/doku.php?id=specificaties:start. This
contains the specifications and requirements for the project. 
3 - Javadoc, at https://survey.erasmusmc.nl/ntd/apidocs/. This contains code 
comments, mostly interesting for programmers, but specifically ScheduleAction
and TestAction classes contain documentation on the tags to use in dsx files.

WARNING:
the tests-tag in dsx files doesn't allow test-tags as children anymore. See
https://survey.erasmusmc.nl/ntd/apidocs/nl/erasmusmc/mgz/ntd/stateMachine/impl/TestsAction.html

WARNING: 
In contrast to prior examples, the construct
<transition ... event="*.negative" />
DOES NOT WORK. The * sign only works at the end of a string, not in the middel and not at 
the start.

So check your dsx files on this construct, and replace the star by the id name of the 
test tag.  


Running the program. 
====================

There is a shell script for mac users (run.sh), but as I don't have mac, I cannot test if it works. 

For windows, linux or mac you can run the program from the command line. Open a command prompt, 
and type in the following: 

java -jar ntdsim.jar

This will give you an overview of command line options. 

Running the program with input: 

java -jar ntdsim.jar -iinput/ntdsim-example.xml -oinput/outputDef.xml -r0-1

Make sure the path indications for input files are correct; the path is relative for the directory in
which the program file is located. 
Also be aware that in the computer world, a file path with spaces in it will fail, unless you place it 
between "". This is because the computer will think that the file name ends when encountering the 
first space. So: -i"filename path with spaces.xml"

Output will be generated in the directory /output under the directory in which the program file 
is located.

If needed, a batch file can be provided for windows users to easily run the program without having to 
type the long command line every time - contact Rinke about this.

If you need to run the program with a large number of agents: 
--------------------------------------------------------------
At present the program has been tested with half a million persons. That took 1.45 hours. More should be 
possible, but only after rewriting, so not with the present version. 
To run the person with a large number of persons, you need to tell java to reserve extra memory. 

java -Xmx7.5g -XX:-UseGCOverheadLimit -jar ntdsim.jar -iinput/ntdsim-example.xml -oinput/outputDef.xml -r0-1

The extra parameters for java tell jave to use extra memory, and to switch of safety measures about you're 
program taking a lot of time. 


WARNINGS
============
The program logs warnings to the console in case of any problems or strange situations
encountered. 
A log warning with the text "Ignoring unknown or invalid element <include>..." 
will most likely be produced by the program, but you can ignore that safely. It will 
be printed in a block of text that looks like this:  

jun 29, 2015 5:21:43 PM org.apache.commons.scxml2.io.SCXMLReader reportIgnoredElement
WARNING: Ignoring unknown or invalid element <include> in namespace "http://www.w3.org/2001/XInclude" as child of <scxml> at Line number = 24
Column number = 36
System Id = null
Public Id = null
Location Uri= null
CharacterOffset = 1050



XML / DSX FILES
===============

The input files are all xml files, even if they have a .dsx extension in stead of 
a xml extension. 
XML is eXtended Markup Language, and these files can be validated before use. There
is absolutely no need to do this, but if you made a lot of changes in the structure, 
it might be sensible to do this. 
Validating goes against xsd schema's. These schema's contain the definitions to which
an xml file should be validated. How to do this is beyond the scope of this documentation;
there is plenty of information on the internet on this. 
 
The schema files are included in the /input/schema directory. Even if you don't use
them for validation, they can be used as a form of documentation, to look up what 
is allowed and what is not allowed in the input files. 


HOW TO MAKE THE PROGRAM REPORT SCREENING
========================================
There is an issue with the software about screening. It is very ease to define screening
in such a way that the combination of numbers for different age/sex specific compliance, 
the run coverage and the fraction excluded is not logically possible. In such a case
the program adapts the age/sex specific compliance, but new is that it now logs a warning
about this. A future version will report the exact numbers about such adaptions, but 
for now this is not implemented yet. 
