All files in this folder (and subfolders) can be used to run a simulation; 
they will NOT be distributed inside the jar file. This directory can be used 
to export to the R-Shiny package or with a bundled zipfile containing the jar 
and some example input xml files and dsx files.

Please organize files here in subdirectories; each disease should get its own
subdirectory, and no files should exist in the root of this folder (except this
readme file).