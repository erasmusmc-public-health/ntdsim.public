<?xml version="1.0" encoding="UTF-8"?>
<!-- This file contains the xsd definition for the output of ntdsim. -->
<xs:schema 
    xmlns="http://erasmusmc.nl/mgz/ntd/output"
    targetNamespace="http://erasmusmc.nl/mgz/ntd/output"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:jaxb="http://java.sun.com/xml/ns/jaxb"
    xmlns:inheritance="http://jaxb2-commons.dev.java.net/basic/inheritance"
    jaxb:extensionBindingPrefixes="inheritance" 
    elementFormDefault="qualified" 
    jaxb:version="2.1">

    <xs:include schemaLocation="sim-commons/commonTypes.xsd"/>
    <xs:include schemaLocation="sim-commons/coreOutputDef.xsd" /> 
    
    <!-- ########################### Main Entry ##############################    -->

    <xs:element name="output">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    java.io.Serializable
                </inheritance:implements>
                <inheritance:implements>
                    nl.erasmusmc.mgz.core.output.generation.IXmlCoreOutput
                </inheritance:implements>
                <inheritance:implements>
                    nl.erasmusmc.mgz.ntd.simulation.output.model.IXmlOutputStateProvider
                </inheritance:implements>  
                <inheritance:extends>
                    nl.erasmusmc.mgz.ntd.simulation.output.impl.XmlAbstractOutput
                </inheritance:extends>          
                <jaxb:class>
                    <jaxb:javadoc>
Defines the form and content of the output. This root class will contain all elements which
are specified in the outputDef.xml file.
&lt;p&gt;In the outputDef.xml file, this tag is the root tag of the xml document. 
&lt;p&gt;&lt;b&gt;Example:&lt;/b&gt;&lt;br&gt;
&amp;lt;output &lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
xmlns:vc="http://www.w3.org/2007/XMLSchema-versioning"&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
xmlns:jaxb="http://java.sun.com/xml/ns/jaxb"&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
xmlns:inheritance="http://jaxb2-commons.dev.java.net/basic/inheritance"&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
xsi:noNamespaceSchemaLocation="../../../main/resources/outputDef.xsd"&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
... other elements go here...&lt;br&gt;
&amp;lt;/output&amp;gt;
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:complexType>
            <xs:sequence>
                <!-- we don't inherit the sim-commons output element here via xs:extend
                    because that gives problems with the inheritance of AbstractOutput, as 
                    java classes cannot extend two parent classes. So we just repeat the 
                    sim-commons output elements here.  -->
                <xs:element name="periodic" minOccurs="0" type="core.output.periodic"/>
                <xs:element name="additional" minOccurs="0" type="core.output.additional.moment.table"/>
                <xs:element name="age.table" type="core.output.age.table"/>
                <xs:element name="state.table" minOccurs="0" type="output.state.table">
                    <xs:annotation>
                        <xs:appinfo>
                            <jaxb:property name="generalStateTable">
                                <jaxb:javadoc>
                                    Defines which states are used for the output. 
                                    The states to which this refers are states which are defined in the state machine. See javadoc on 
                                    the type for more explanation.
                                </jaxb:javadoc>
                            </jaxb:property>
                        </xs:appinfo>
                    </xs:annotation>
                </xs:element>
                <xs:element name="elements" type="output.elements"/>
            </xs:sequence>
            <xs:attribute name="sex" use="optional" default="false" type="xs:boolean"/>
        </xs:complexType>
    </xs:element>
    
    <!-- -  -  -  -  -  -  -  -  - States -  -  -  -  -  -  -  -  -   -->
    <xs:complexType name="output.state.table">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    java.io.Serializable
                </inheritance:implements>
                <jaxb:class>
                    <jaxb:javadoc>
Defines which states are used for the output. Depending on the output elements, each state
will get separate column(s) in the output.&lt;br&gt; 
The states to which this refers are states which are defined in the state machine, in
the dsx file which is referenced in the /ntdsimInput/simulation/dsx element of the general input file.
For each state tag, you can define a single state machine state. Note however that this
state table can be overruled by specifying output states on the elements in this 
input file (/output/elements/[elementName]/).
&lt;p&gt;In the outputDef.xml file, this tag is placed inside the root output-tag. 
&lt;p&gt;&lt;b&gt;Example:&lt;/b&gt;&lt;br&gt;
&amp;lt;output ... &amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;state.table&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;state name="susceptible" /&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;state name="alive" /&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;/state.table&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
... other elements go here...&lt;br&gt;
&amp;lt;/output&amp;gt;
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:element maxOccurs="unbounded" name="state" type="output.state">
                <xs:annotation>
                    <xs:appinfo>
                        <jaxb:property name="states"/>
                    </xs:appinfo>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="output.state">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    java.io.Serializable
                </inheritance:implements>
                <inheritance:implements>
                    nl.erasmusmc.mgz.ntd.simulation.output.model.IOutputState
                </inheritance:implements>  
                <inheritance:extends>
                    nl.erasmusmc.mgz.ntd.simulation.output.impl.XmlAbstractOutputState
                </inheritance:extends>  
                <jaxb:class>
                    <jaxb:javadoc>
Defines on which states the output is generated. You can simply list normal state machine states
defined in any of the relevant dsx files. This type allows specifying extra attributes
for conditions (and one for representation).&lt;br&gt; 
Note that, in contrast to prior versions, the state name isn't placed anymore between state tags
(&amp;lt;state&amp;gt;susceptible&amp;lt;/state&amp;gt;). In stead, the name is placed in a specific
name attribute (&amp;lt;state name="sick" /&amp;gt;).&lt;p&gt;
For each state tag's content you can define a single state machine state (via the name attribute). 
All other attributes are optionally; you don't have to specify any. 
&lt;p&gt;In the outputDef.xml file, this tag can be used on the general state list (/output/state.table), 
and on prevalence (/output/elements/prevalence) and vector (/output/elements/vector). You can use it
more than once in all of these locations.&lt;p&gt;
There are four attributes (include_states, exclude_states, include_memory and exclude_memory) which are 
conditional attributes. It means that they specify a condition to which an agent must adhere in order to
have the output event included in the output. The following rules apply for these conditional attributes:
&lt;ul&gt;
&lt;li&gt;The attribute's content may be a single state or a list of state machine states.
In case of specifying a list of states, the states in such a list are always combined on an OR-basis. 
This means that the construct&lt;br&gt;
&amp;lt;state include_states="bla1 bla2" name="sick" /&amp;gt;&lt;br&gt;
will only score (for example prevalences) for state sick under the condition that the agents are in 
state bla1 OR state bla2. (A future version may make it possible to provide these conditions on an AND-basis). 
&lt;li&gt;The states listed here are not included in the output generated, but they serve as a condition. 
For example: in case of used in incidence, the construct&lt;br&gt; 
&amp;lt;state from="healthy" include_states="bla" name="sick" /&amp;gt;&lt;br&gt;
means that incidences from healthy to sick are recorded for the output, but only under the condition that
the agent was in the bla state when the incidence happened. 
&lt;li&gt;The states listed in these conditional attributes may not be related to the output state or from 
state. Not related means they may not be a parent or a child state of the output state (or from state). 
States who are listed in these attributes who fail to meet this condition are simply ignored. In practice, 
this rule means that it only makes sense to list states in such an attribute if the are states who are 
parallel to the output subject state. 
&lt;/ul&gt;
&lt;p&gt;&lt;b&gt;Example of usage:&lt;/b&gt;&lt;br&gt;
&amp;lt;output ... &amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;elements&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;prevalence&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;state name="susceptible" /&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;state name="sick" include_states="bla" exclude_states="heavy" /&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;state name="stage1" include_memory="low" include_states="high" exclude_memory="big" /&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;/prevalence&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;/elements&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
... other items go here...&lt;br&gt;
&amp;lt;/output&amp;gt;
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:attributeGroup ref="output.state.attributes"/>
    </xs:complexType>
    
    <xs:attributeGroup name="output.state.attributes">
        <xs:attribute name="name" use="required" type="xs:token">
            <xs:annotation>
                <xs:appinfo>
                    <jaxb:property>
                        <jaxb:javadoc>
The id of the state on which output is to be generated. In case of incidence this is 
always the to-state. On all other cases there is only one state involved. This property must
be a single state id. No list of states is allowed. 
                        </jaxb:javadoc>
                    </jaxb:property>
                </xs:appinfo>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="include_states" type="xs:token">
            <xs:annotation>
                <xs:appinfo>
                    <jaxb:property>
                        <jaxb:javadoc>
This is a conditional property, meaning that it defines a condition for generating the output
on the specified state. See the javadoc for the containing class for additional rules.&lt;br&gt;
With this property you can define the states where the agent should be in at the moment of the 
output probing. The output for this output subject state will only be generated if the agent 
is in any state listed in this attribute.&lt;br&gt;
                        </jaxb:javadoc>
                    </jaxb:property>
                </xs:appinfo>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="exclude_states" type="xs:token">
            <xs:annotation>
                <xs:appinfo>
                    <jaxb:property>
                        <jaxb:javadoc>
This is a conditional property, meaning that it defines a condition for generating the output
on the specified state. See the javadoc for the containing class for additional rules.&lt;br&gt;
With this property you can define the states where the agent should NOT be in at the moment of the 
output probing. The output for this output subject state will only be generated if the agent 
is NOT in all of the states listed in this attribute.&lt;br&gt;
                        </jaxb:javadoc>
                    </jaxb:property>
                </xs:appinfo>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="include_memory" type="xs:token">
            <xs:annotation>
                <xs:appinfo>
                    <jaxb:property>
                        <jaxb:javadoc>
This is a conditional property, meaning that it defines a condition for generating the output
on the specified state. See the javadoc for the containing class for additional rules.&lt;br&gt;
With this property you can define the states which should be in the agent's memory at the moment of the 
output probing. The output for this output subject state will only be generated if the agent 
has any state listed in this attribute in its memory.&lt;br&gt;
                        </jaxb:javadoc>
                    </jaxb:property>
                </xs:appinfo>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="exclude_memory" type="xs:token">
            <xs:annotation>
                <xs:appinfo>
                    <jaxb:property>
                        <jaxb:javadoc>
This is a conditional property, meaning that it defines a condition for generating the output
on the specified state. See the javadoc for the containing class for additional rules.&lt;br&gt;
With this property you can define the states which the agent should NOT have in its memory at the moment of the 
output probing. The output for this output subject state will only be generated if the agent 
has NOT any of the states listed in this attribute in its memory.&lt;br&gt;
                        </jaxb:javadoc>
                    </jaxb:property>
                </xs:appinfo>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="conditionLabel" default="" type="xs:token">
            <xs:annotation>
                <xs:appinfo>
                    <jaxb:property>
                        <jaxb:javadoc>
When any of the conditional attributes is used, it is wise to specify this attribute. 
It is a string which is placed in the output in the condition column. Not specifying this
may lead to output in which it is not clear what different rows mean, and where the conditions
under which output lines are generated are unknown. 
                        </jaxb:javadoc>
                    </jaxb:property>
                </xs:appinfo>
            </xs:annotation>
        </xs:attribute>
    </xs:attributeGroup>
    

    <!-- -  -  -  -  -  -  -  -  - Output content -  -  -  -  -  -  -  -  -   -->
    <xs:complexType name="output.elements">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    java.io.Serializable
                </inheritance:implements>
                <jaxb:class>
                    <jaxb:javadoc>
Defines which elements will be included in the output. User can choose from
any combination of output.element, where each outputElement contains the 
definition of one or more output columns. 
&lt;p&gt;In the outputDef.xml file, this tag is placed inside the root output-tag. 
&lt;p&gt;&lt;b&gt;Example:&lt;/b&gt;&lt;br&gt;
&amp;lt;output ... &amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;elements&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;prevalence /&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;incidence /&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;vector /&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;/elements&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
... other items go here...&lt;br&gt;
&amp;lt;/output&amp;gt;
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:element minOccurs="0" name="prevalence" type="output.element.prevalence"/>
            <xs:element minOccurs="0" name="incidence" type="output.element.incidence"/>
            <xs:element minOccurs="0" name="vector" type="output.element.vector"/>
            <xs:element minOccurs="0" name="participation" type="output.element.participation"/>
            <xs:element minOccurs="0" name="households" type="output.element.households"/>
            <xs:element minOccurs="0" name="custom" type="output.element.custom"/>
        </xs:sequence>
    </xs:complexType>

    <!-- -  -  -  -  -  -  -  -  - Prevalence -  -  -  -  -  -  -  -  -   -->
    <xs:complexType name="output.element.prevalence">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    java.io.Serializable
                </inheritance:implements>
                <jaxb:class>
                    <jaxb:javadoc>
When this element is set, it means that prevalence output is generated. Attributes and child
elements may be used to configure the output on prevalence. For each state tag, you can 
define a single state machine state. If you don't define states, the gerenal state table is used
(/output/state.table).
&lt;p&gt;In the outputDef.xml file, this tag is placed inside the elements-tag. 
&lt;p&gt;&lt;b&gt;Example:&lt;/b&gt;&lt;br&gt;
&amp;lt;output ... &amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;elements&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;prevalence&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;state name="susceptible" /&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;state name="alive" /&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;/prevalence&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;/elements&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
... other items go here...&lt;br&gt;
&amp;lt;/output&amp;gt;
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:element maxOccurs="unbounded" minOccurs="0" name="state" type="output.state">
                <xs:annotation>
                    <xs:appinfo>
                        <jaxb:property name="states"/>
                    </xs:appinfo>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
    </xs:complexType>
    
    <!-- -  -  -  -  -  -  -  -  - incidence -  -  -  -  -  -  -  -  -   -->
    <xs:complexType name="output.element.incidence">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    java.io.Serializable
                </inheritance:implements>
                <jaxb:class>
                    <jaxb:javadoc>
When this element is set, it means that incidence output is generated. Attributes and child
elements may be used to configure the output on incidence. 
If you don't define states, the gerenal state table is used
(/output/state.table).
&lt;p&gt;In the outputDef.xml file, this tag is placed inside the elements-tag. 
&lt;p&gt;&lt;b&gt;Example:&lt;/b&gt;&lt;br&gt;
&amp;lt;output ... &amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;elements&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;incidence personYears="true"&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;state name="susceptible" /&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;state name="alive" /&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;/incidence&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;/elements&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
... other items go here...&lt;br&gt;
&amp;lt;/output&amp;gt;
&lt;p&gt;Note that several extra attributes on states are possible here. See the state type for more examples.
&lt;p&gt;Note that currently there is no way to get the number of newborns directly from incidence (or
prevalence). However, there are ways around this: you could get the incidence of death and the prevalence of alive, 
and from this calculate the number of newborns. Another workaround is to create a special "newborn" state in the
disease biology dsx, and let all agents enter in that state when they start life. The state can then directly 
forward to another state (i.e. susceptible). 
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:element maxOccurs="unbounded" minOccurs="0" name="state" type="output.element.incidence.state">
                <xs:annotation>
                    <xs:appinfo>
                        <jaxb:property name="states"/>
                    </xs:appinfo>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
        <xs:attribute name="personYears" default="true" type="xs:boolean">
            <xs:annotation>
                <xs:appinfo>
                    <jaxb:property>
                        <jaxb:javadoc>
When this attribute is true, personYears output is also generated. The default value 
is true. Note however that if it is true, the personYears attribute on the state 
allows you to switch off personyears output on particular states.&lt;br&gt; 
When this personYears attribute is false, it overrides the personYears attribute 
on the states: these are then ignored.  
                        </jaxb:javadoc>
                    </jaxb:property>
                </xs:appinfo>
            </xs:annotation>
        </xs:attribute>
    </xs:complexType>

    <xs:complexType name="output.element.incidence.state">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    nl.erasmusmc.mgz.ntd.simulation.output.model.IOutputIncidenceState
                </inheritance:implements>  
                <inheritance:extends>
                    nl.erasmusmc.mgz.ntd.simulation.output.impl.XmlAbstractOutputState
                </inheritance:extends>  
                <jaxb:class>
                    <jaxb:javadoc>
Defines on which states the incidence output is generated. A state listed here will be considered 
as a to-state, so a state which is entered by the agent. From states are separately
specified by the from attribute on the to-state. &lt;br&gt; 
When any state is set, this overrules the general settings for states on output. Note that when 
no states are specified, the general state settings for output are used. This means that any 
combination of from-states and to-states will be included in the output, provided that both 
from and to state are listed under /output/state.table.&lt;br&gt;
Personyears output is done only on the to-state, not on the from-state. &lt;p&gt;
For each state tag's content you can define a single state machine state. All attributes are
optionally; you don't have to specify any. The example lists several possibilities.
&lt;p&gt;In the outputDef.xml file, this tag is placed inside the incidence-tag. 
&lt;p&gt;&lt;b&gt;Example:&lt;/b&gt;&lt;br&gt;
&amp;lt;output ... &amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;elements&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;incidence&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;state name="susceptible" /&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;state name="alive" from="*" combine="false" /&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;state name="stage1" from="*" combine="true" /&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;state name="stage2" from="stage0 stage1" combine="false" /&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;/incidence&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;/elements&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
... other items go here...&lt;br&gt;
&amp;lt;/output&amp;gt;
&lt;p&gt;Note that several extra attributes on states are possible here. See the state type for more examples.
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:attributeGroup ref="output.state.attributes"/>
        <xs:attribute name="from" default="*" type="xs:token">
            <xs:annotation>
                <xs:appinfo>
                    <jaxb:property>
                        <jaxb:javadoc>
With this property you can define the states where the agent is coming from when entering  
this state. If this attribute is specified, only incidences coming from the specified from states
to this state are included in output. For personyears the attribute is ignored.&lt;br&gt;
The from attribute must obey one of the following formats:
&lt;ul&gt;
&lt;li&gt;The name of a single state machine state, as defined in the dsx file (or its child files).
&lt;li&gt;A space separated list of state names as described by the previous item.
&lt;li&gt;*, meaning that all possible states will be used as from-state. 
&lt;/ul&gt;
When the from attribute is not specified, the * option will be used as default.
Note that when no state is specified under incidence, the default setting in output/state.table is used.
                        </jaxb:javadoc>
                    </jaxb:property>
                </xs:appinfo>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="combine" default="true" type="xs:boolean">
            <xs:annotation>
                <xs:appinfo>
                    <jaxb:property>
                        <jaxb:javadoc>
When the combine attribute is true, all from-states are combined in one line of output for each to-state. 
When the attribute is false, each specified from-state for this to-state gets a separate line in the
output. This only counts for the incidence table in output, not for the personYears table.
                        </jaxb:javadoc>
                    </jaxb:property>
                </xs:appinfo>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="incidence" default="true" type="xs:boolean">
            <xs:annotation>
                <xs:appinfo>
                    <jaxb:property>
                        <jaxb:javadoc>
When this attribute is true, incidence output is generated for this state.  
When the attribute is false, this state is not included in the incidence output.
As the default is true, normally you wouldn't specify this attribute. It only 
makes sense when you set it to false, and set the personYears attribute to true. 
In that case, the state isn't included in the incidence output table, but it is
in the personYears output table. &lt;br&gt;
When both @incidence and @personYears attributes are false, this state is ignored. 
When @incidence is false, the @from and @combine attributes are ignored. 
                        </jaxb:javadoc>
                    </jaxb:property>
                </xs:appinfo>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="personYears" default="true" type="xs:boolean">
            <xs:annotation>
                <xs:appinfo>
                    <jaxb:property>
                        <jaxb:javadoc>
When this attribute is true, personYears output is also generated for this state.  
When the attribute is false, this state is not included in the personYears output. 
This attribute is ignored when you specified not to generate personYears output
on the incidence element (output/elements/incidence/@personYears=false). 
                        </jaxb:javadoc>
                    </jaxb:property>
                </xs:appinfo>
            </xs:annotation>
        </xs:attribute>
    </xs:complexType>
    
    <!-- -  -  -  -  -  -  -  -  - Vector -  -  -  -  -  -  -  -  -   -->
    <xs:complexType name="output.element.vector">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    java.io.Serializable
                </inheritance:implements>
                <jaxb:class>
                    <jaxb:javadoc>
When this element is set, it means that vector output is generated. Attributes and child
elements may be used to configure the output on vector. &lt;br&gt;
States only apply to the ToVectorForceOfInfection output - the VectorBites output doesn't 
use states. The state elements are ignored when the toVectorFOI attribute is false.  
When both @vectorBites and @toVectorFOI attributes are false, this vector element is
ignored and no output on vector will be generated. If you don't define states, 
the gerenal state table is used (/output/state.table) when needed.
&lt;p&gt;In the outputDef.xml file, this tag is placed inside the elements-tag. 
&lt;p&gt;&lt;b&gt;Example:&lt;/b&gt;&lt;br&gt;
&amp;lt;output ... &amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;elements&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;vector vectorBites="true" toVectorFOI="true"&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;state name="susceptible" /&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;state name="alive" /&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;/vector&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&amp;lt;/elements&amp;gt;&lt;br&gt;
&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
... other items go here...&lt;br&gt;
&amp;lt;/output&amp;gt;
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:element maxOccurs="unbounded" minOccurs="0" name="state" type="output.state">
                <xs:annotation>
                    <xs:appinfo>
                        <jaxb:property name="states"/>
                    </xs:appinfo>
                </xs:annotation>
            </xs:element>
        </xs:sequence>
        <xs:attribute name="vectorBites" default="true" type="xs:boolean">
            <xs:annotation>
                <xs:appinfo>
                    <jaxb:property>
                        <jaxb:javadoc>
When this attribute is true, output on vector bites is generated.   
                        </jaxb:javadoc>
                    </jaxb:property>
                </xs:appinfo>
            </xs:annotation>
        </xs:attribute>
        <xs:attribute name="toVectorFOI" default="true" type="xs:boolean">
            <xs:annotation>
                <xs:appinfo>
                    <jaxb:property>
                        <jaxb:javadoc>
When this attribute is true, output on the to-vector-force-of-infection is generated.   
                        </jaxb:javadoc>
                    </jaxb:property>
                </xs:appinfo>
            </xs:annotation>
        </xs:attribute>
    </xs:complexType>
    
    <!-- -  -  -  -  -  -  -  -  - Participation -  -  -  -  -  -  -  -  -   -->
    <xs:complexType name="output.element.participation">
        <xs:annotation>
            <xs:appinfo>
                <jaxb:class>
                    <jaxb:javadoc>
                        When this element is set, it means that participation output is generated. It has not attributes nor sub-elements.
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
    </xs:complexType>
    
    <!-- -  -  -  -  -  -  -  -  - Households -  -  -  -  -  -  -  -  -   -->
    <xs:complexType name="output.element.households">
        <xs:sequence>
            <xs:element minOccurs="0" name="householdSizes" type="output.element.households.sizes"/>
            <xs:element minOccurs="0" name="householdTypes" type="output.element.households.types"/>
            <xs:element minOccurs="0" name="numbersMarried" type="output.element.households.numbersMarried"/>
            <xs:element minOccurs="0" name="marriedCrossTable" type="output.element.households.marriedCrossTable"/>
        </xs:sequence>
    </xs:complexType>
    
    <xs:complexType name="output.element.households.sizes">
        <!--        <xs:sequence>
            <xs:element maxOccurs="unbounded" minOccurs="0" name="moment" type="core.output.additional.moment">
                <xs:annotation>
                    <xs:appinfo>
                        <jaxb:property name="moments"/>
                    </xs:appinfo>
                </xs:annotation>
            </xs:element>
        </xs:sequence>  -->
    </xs:complexType>
    
    <xs:complexType name="output.element.households.types" />
    <xs:complexType name="output.element.households.numbersMarried" />
    <xs:complexType name="output.element.households.marriedCrossTable" />
    
    <!-- -  -  -  -  -  -  -  -  - Custom -  -  -  -  -  -  -  -  -   -->
    <xs:complexType name="output.element.custom">
        <xs:annotation>
            <xs:appinfo>
                <inheritance:implements>
                    java.io.Serializable
                </inheritance:implements>
                <jaxb:class>
                    <jaxb:javadoc>
This element is only used for testing purposes. It doesn't have any content. 
                    </jaxb:javadoc>
                </jaxb:class>
            </xs:appinfo>
        </xs:annotation>
    </xs:complexType>

    <!--
    <xs:simpleType name="output.element.type">
        <xs:restriction base="xs:token">
            <xs:enumeration value="PREVALENCE"/>
            <xs:enumeration value="INCIDENCE"/>
            <xs:enumeration value="VECTOR"/>
            <xs:enumeration value="CUSTOM"/>
        </xs:restriction>
    </xs:simpleType>
    -->

</xs:schema>
