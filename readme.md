# NTDSim public repository

This is the public repository for the NTDSIM project. 

The goal of the project is to develop software that 
implements an individual-based simulation model 
that can be used to predict prevented disease 
and death due to 
NTDs (Neglected Tropical Diseases).

## For Downloads of releases:
 
* go to the *tags* link just below the *Ntdsim.public* heading on this page
* select a tag/version
* click the download button at the right and select a format.

## For documentation:

Please refer to the wiki, use the *wiki* button in the left menu. 

